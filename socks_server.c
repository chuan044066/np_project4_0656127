#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <fcntl.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include "inet.h"

#define BIND_PORT "4859"
#define PORT "6857"
#define BUFFERSIZE 8000

struct request_info{
    unsigned char VN;
    unsigned char CD;
    unsigned int DST_PORT;
    unsigned int DST_IP;
    char *USER_ID;
    int fire_flag;
};

int socks4_process(int conn_fd);
int socks4_request(int conn_fd, struct request_info *req);
int connect_mode(int conn_fd, const struct request_info req);
int bind_mode(int conn_fd);
int data_bridge(int conn_fd, int dst_fd);

int main(int argc, char *argv[]){

    int server_fd = 0;
    int conn_fd = 0;

    server_fd = passive_tcp(NULL, PORT);
    while(1){
        int childpid = 0;
        conn_fd = accept_tcp(server_fd);

        if((childpid = fork()) < 0){
            fputs("ERROR fork\n", stderr);
        }
        else if(childpid == 0){
            close(server_fd);
            socks4_process(conn_fd);
            exit(0);
        }
        else{
            close(conn_fd);
        }
    }

    close(server_fd);
    return 0;
}

int socks4_process(int conn_fd){
    struct request_info req;
    int dst_fd = 0;

    memset(&req, 0, sizeof(struct request_info));

    socks4_request(conn_fd, &req);

    if(req.CD == 1){   //connect mode
        dst_fd = connect_mode(conn_fd, req);

        if (dst_fd < 0)
        {
            printf("SOCKS_CONNECT FAILED ....\n");
            exit(-1);
        }
        printf("SOCKS_CONNECT GRANTED ....\n");
    }
    else if(req.CD == 2){  //bind mode6803
        dst_fd = bind_mode(conn_fd);

        if (dst_fd < 0)
        {
            printf("BIND_LISTEN FAILED ....\n");
            exit(-1);
        }
        printf("BIND_LISTEN GRANTED ....\n");
    }

    data_bridge(conn_fd, dst_fd);
    return 0;
}

int socks4_request(int conn_fd, struct request_info *req){
    unsigned char buffer[BUFFERSIZE] = {0};

    char *source_ip = (char *)malloc(NI_MAXHOST);
    memset(source_ip, 0, NI_MAXHOST);


    read(conn_fd, buffer, BUFFERSIZE);

    req->VN = buffer[0];
    req->CD = buffer[1];
    req->DST_PORT = buffer[2] << 8 | buffer[3];  //big endian to little
    req->DST_IP = buffer[7] << 24 | buffer[6] << 16 | buffer[5] << 8 | buffer[4];
    req->USER_ID = buffer + 8;

    strncpy(source_ip, fd_get_ip_addr(conn_fd), NI_MAXHOST);

    struct sockaddr_in sin;
    socklen_t len = sizeof(sin);
    if (getsockname(conn_fd, (struct sockaddr *)&sin, &len) == -1)
    perror("getsockname");



    FILE *firewall_fd;
    if((firewall_fd = fopen("socks.conf", "r")) == NULL){
        printf("fail to read conf");
    }

    char addr_string[25];
    unsigned char addr[4]; //permit ip
    char *pch;
    while(fgets(addr_string, 25, firewall_fd) != NULL){

        //printf("%s\n", addr_string);
        pch = strtok(addr_string, " ");
        char *mode = strtok(NULL, " ");
        pch = strtok(NULL, ".");
        addr[0] = (unsigned char)atoi(pch);
        pch = strtok(NULL, ".");
        addr[1] = (unsigned char)atoi(pch);
        pch = strtok(NULL, ".");
        addr[2] = (unsigned char)atoi(pch);
        pch = strtok(NULL, ".");
        addr[3] = (unsigned char)atoi(pch);

        //printf("%s\n", mode);
        //printf("%u.%u.%u.%u\n", addr[0], addr[1], addr[2], addr[3]);

        if((!strcmp(mode, "c") && req->CD == 1) || (!strcmp(mode, "b") && req->CD == 2)){
            if( ((addr[0] == buffer[4])||(addr[0] == 0)) && ((addr[1] == buffer[5])||(addr[1] == 0))
            && ((addr[2] == buffer[6])||(addr[2] == 0)) && ((addr[3] == buffer[7])||(addr[3] == 0)) ){
                req->fire_flag = 1;
                break;
            }
        }

    }
    fclose(firewall_fd);

    printf("------------request-----------\n");
    printf("<S_IP>   :%s\n", source_ip);
    printf("<S_PORT> :%d\n", ntohs(sin.sin_port));
    printf("<D_IP>   :%u.%u.%u.%u\n", buffer[4], buffer[5], buffer[6], buffer[7]);
    printf("<D_port> :%u\n", req->DST_PORT);
    printf("<Command>: %s \n",(req->CD == 2)? "BIND":"CONNECT");
    printf("<Content>: %u\n", buffer[0]);
    if(req->fire_flag == 1){
        printf("<Reply>  :Accept\n");
    }
    else{
        printf("<Reply>  :Reject\n");
    }
    printf("------------------------------\n");


    return 0;
}

int connect_mode(int conn_fd, const struct request_info req)
{
    unsigned char reply[8] = {0};
    int dst_fd = 0;
    char dst_ip[16] = {0};
    char dst_port[10] = {0};

    reply[0] = 0;
    if(req.fire_flag == 1){
        reply[1] = 0x5A;
    }
    else{
        reply[1] = 91;
    }
    reply[2] = req.DST_PORT / 256;
    reply[3] = req.DST_PORT % 256;
    reply[4] = (req.DST_IP >> 24) & 0xFF;
    reply[5] = (req.DST_IP >> 16) & 0xFF;
    reply[6] = (req.DST_IP >> 8) & 0xFF;
    reply[7] = req.DST_IP & 0xFF;

    snprintf(dst_ip, 16, "%u.%u.%u.%u", reply[7], reply[6], reply[5], reply[4]);
    snprintf(dst_port, 10, "%u", req.DST_PORT);

    if(req.fire_flag == 1){
        dst_fd = active_tcp(dst_ip, dst_port);
    }
    if (dst_fd < 0)
    {
        perror("Connection:");
        return -1;
    }
    write(conn_fd, reply, 8);

    return dst_fd;
}

int bind_mode(int conn_fd)
{
    char reply[8] = {0};
    int dst_fd = 0;
    int bind_fd = 0;
    struct sockaddr_in bind_addr;
    socklen_t bind_addr_len = sizeof(bind_addr);

    memset(&bind_addr, 0, sizeof(bind_addr));

    //bind port
    bind_fd = passive_tcp(NULL, BIND_PORT);



    if (getsockname(bind_fd, (struct sockaddr *)&bind_addr, &bind_addr_len) < 0)
    {
        perror("getsockname error");
        exit(-1);
    }


    unsigned int dst_port = ntohs(bind_addr.sin_port);

    reply[0] = 0;
    reply[1] = 0x5A;
    reply[2] = dst_port / 256;
    reply[3] = dst_port % 256;
    reply[4] = 0;
    reply[5] = 0;
    reply[6] = 0;
    reply[7] = 0;

    write(conn_fd, reply, 8);   //write reply

    dst_fd = accept_tcp(bind_fd);

    write(conn_fd, reply, 8);

    return dst_fd;
}

int data_bridge(int conn_fd, int dst_fd)
{
    fd_set arfds, rfds;
    int nfds;
    char buffer[BUFFERSIZE] = {0};

    FD_ZERO(&rfds);
    FD_ZERO(&arfds);

    FD_SET(conn_fd, &arfds);
    FD_SET(dst_fd, &arfds);

    nfds = getdtablesize();

    while (1)
    {
        int len = 0;
        memcpy(&rfds, &arfds, sizeof(rfds));
        memset(buffer, 0, BUFFERSIZE);


        if (select(nfds + 1, &rfds, NULL, NULL, NULL) < 0 && errno != EINTR)
        {
            perror("select error");
            close(conn_fd);
            close(dst_fd);
            exit(EXIT_FAILURE);
        }
        if (FD_ISSET(conn_fd, &rfds))         //browser can read
        {
            len = read(conn_fd, buffer, sizeof(buffer));
            if (len <= 0)
            {
                close(conn_fd);
                close(dst_fd);
                break;
            }
            else
            write(dst_fd, buffer, len);   //write to dst
        }
        if (FD_ISSET(dst_fd, &rfds))          //dst can read
        {
            len = read(dst_fd, buffer, sizeof(buffer));
            if (len <= 0)
            {
                close(conn_fd);
                close(dst_fd);
                break;
            }
            else
            write(conn_fd, buffer, len);    //write to browser
        }

    }
    return 0;
}
