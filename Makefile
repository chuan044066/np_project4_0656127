CC := gcc
CFLAGS := -g -Wall

all: socks_server

socks_server: socks_server.o inet.o
		$(CC) $(CFLAGS) $^ -o $@

%.o: %.c %.h
	$(CC) $(CFLAGS) -c $<

.PHONY : clean
clean:
		-rm -f *.o socks_server 
