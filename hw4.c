#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <sys/uio.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#include "include/str.h"
#include "include/inet.h"

#define Max_clients_number 5
#define BUFFERSIZE 15000
#define F_CONNECTING 0
#define F_READING 1
#define F_WRITING 2
#define F_DONE 3

struct clients_connects
{
    char ip[16];
    char hostname[100];
    char port[6];
    char file_name[100];

    char sockip[16];
    char sockhostname[100];
    char sockport[6];

    int conn_fd;
    FILE *input_fp;
    int status;
    int file_fd;
};
struct clients_connects table[Max_clients_number];
int readline(int fd,char *ptr,int maxlen, char delim);
int print_html(char *str, int col, int bold);
void html_header();
void token_querystring();
int hostname_to_ip(char * hostname, char* ip);
void socks4_request(int conn_fd, struct clients_connects table);


int main(int argc,char *argv[])
{
    int i = 0;
    char read_buf[BUFFERSIZE];
    int prompt[Max_clients_number];
    int conn_count = 0;
    fd_set rfds;
    fd_set wfds;
    fd_set arfds;
    fd_set awfds;
    int nfds;

    memset(table, 0, sizeof(struct clients_connects) * 5);
    memset(prompt, 0, sizeof(int) * 4);
    FD_ZERO(&rfds);
    FD_ZERO(&wfds);
    FD_ZERO(&arfds);
    FD_ZERO(&awfds);

    html_header();
    token_querystring();

    printf("<table width=\"800\" border=\"1\">\r\n");
    printf("<tr>\n");

    for (i = 0; i < Max_clients_number; i++)
    {
        if (strlen(table[i].ip) != 0 )
        {
            if(strlen(table[i].sockip) != 0){
                table[i].conn_fd = active_tcp(table[i].sockip, table[i].sockport);
                fcntl(table[i].conn_fd, F_SETFL, O_NONBLOCK);
                socks4_request(table[i].conn_fd, table[i]);
            }
            else{
                table[i].conn_fd = active_tcp(table[i].ip, table[i].port);
                fcntl(table[i].conn_fd, F_SETFL, O_NONBLOCK);
            }
            FD_SET(table[i].conn_fd, &arfds);
            FD_SET(table[i].conn_fd, &awfds);
            table[i].file_fd = open(table[i].file_name, O_RDONLY);
            table[i].status = F_CONNECTING;
            conn_count++;
            nfds = table[i].conn_fd;

        }
        else
        {
            table[i].status = F_DONE;
        }
    }


    rfds = arfds;
    wfds = awfds;
    for(int i=0; i < 5; i++) {
        printf("<td>%s</td>\n", table[i].hostname);
    }

    printf("</tr>\r\n");
    printf("<tr>\r\n");

    for (i = 0; i < Max_clients_number; i++)
    {
        printf("<td valign=\"top\" id=\"m%d\"></td>\r\n", i);
    }

    printf("</tr>\r\n");
    printf("</table>");

    while (conn_count > 0)
    {
    memcpy(&rfds, &arfds, sizeof(rfds));
    memcpy(&wfds, &awfds, sizeof(wfds));

    if (select(nfds+1, &rfds, &wfds, NULL, NULL) < 0)
    {
        perror("select");
        exit(EXIT_FAILURE);
    }

    for (i = 0; i < Max_clients_number; i++)
    {
        int len = 0;
        if (table[i].status == F_DONE) continue;
        else if (table[i].status == F_CONNECTING && (FD_ISSET(table[i].conn_fd, &rfds) || FD_ISSET(table[i].conn_fd, &wfds)))
        {
            int error;
            if (getsockopt(table[i].conn_fd, SOL_SOCKET, SO_ERROR, &error, &len) < 0 || error != 0) {
                return (-1);
            }
            table[i].status = F_READING;
            FD_CLR(table[i].conn_fd, &awfds);
        }
        else if (table[i].status == F_READING && FD_ISSET(table[i].conn_fd, &rfds))
        {
            memset(read_buf, 0, BUFFERSIZE);
            len = readline(table[i].conn_fd, read_buf, BUFFERSIZE, '%');
            if(len <= 0)
            {
                table[i].status = F_DONE;
                FD_CLR(table[i].conn_fd, &arfds);
                close(table[i].conn_fd);
                conn_count--;
                continue;
            }

            print_html(read_buf, i, 0);

            if(strchr(read_buf, '%')) {
                table[i].status = F_WRITING;
                FD_CLR(table[i].conn_fd,&arfds);
                FD_SET(table[i].conn_fd,&awfds);
            }


        }
        else if (table[i].status == F_WRITING && FD_ISSET(table[i].conn_fd, &wfds))
        {
            char cmd[BUFFERSIZE] = {'\0'};
            len = readline(table[i].file_fd, cmd, BUFFERSIZE, '%');
            int NeedWrite = len;
            print_html(cmd, i, 1);
            int n=0;
            n = write(table[i].conn_fd, cmd, strlen(cmd));

            NeedWrite -= n;
            if (n <= 0 || NeedWrite <= 0) {
                table[i].status = F_READING;
                FD_CLR(table[i].conn_fd, &awfds);
                FD_SET(table[i].conn_fd, &arfds);
            }
        }
    }
}
    for(int i=0; i<5; i++){
        if(table[i].conn_fd > 0){
            close(table[i].conn_fd);
        }
    }


    printf("%s","</font>\r\n</body>\r\n</html>\r\n");
    fflush(stdout);

return 0;
}

void socks4_request(int conn_fd, struct clients_connects table)
{
    char buffer[8] = {0};
    unsigned int port = atoi(table.port);
    int cnt = 0;
    char **ip = strtok_bychar(table.ip, '.', &cnt);

    buffer[0] = 4;
    buffer[1] = 1;
    buffer[2] = port / 256;
    buffer[3] = port % 256;

    for(int i=0; i < cnt; i++){
        buffer[i + 4] = atoi(ip[i]);
    }

    write(conn_fd, buffer, 8);
    read(conn_fd, buffer, 8);

}

int hostname_to_ip(char * hostname, char* ip)
{
    struct hostent *he;
    struct in_addr **addr_list;
    int i;

    if ( (he = gethostbyname( hostname ) ) == NULL)
    {
        // get the host info
        herror("gethostbyname");
        return 1;
    }

    addr_list = (struct in_addr **) he->h_addr_list;

    for(i = 0; addr_list[i] != NULL; i++)
    {
        //Return the first one;
        strcpy(ip, inet_ntoa(*addr_list[i]) );
        return 0;
    }

    return 1;
}


void token_querystring(){
    char *querystring = getenv("QUERY_STRING");
    char **parse_result;
    int parse_cnt = 0;
    parse_result = strtok_bychar(querystring, '&', &parse_cnt);

    for(int i = 0; i < Max_clients_number; i++) {
        memset(table[i].ip, 0, sizeof table[0].ip);
        memset(table[i].port, 0, sizeof table[0].port);
        memset(table[i].file_name, 0, sizeof table[0].file_name);
    }
    for(int i = 0; i < Max_clients_number; i++) {
        int j= i * 5;
        strcpy(table[i].hostname, strstr(parse_result[j], "=")+1);
        hostname_to_ip(strstr(parse_result[j], "=")+1, table[i].ip);
        strcpy(table[i].port, strstr(parse_result[j+1], "=")+1);
        strcpy(table[i].file_name, strstr(parse_result[j+2], "=")+1);

        strcpy(table[i].sockhostname, strstr(parse_result[j+3], "=")+1);
        hostname_to_ip(strstr(parse_result[j+3], "=")+1, table[i].sockip);
        strcpy(table[i].sockport, strstr(parse_result[j+4], "=")+1);


    }

}
int print_html(char *str, int col, int bold)
{
    char *cptr = str;
    int i = 0;

    printf("<script>document.all['m%d'].innerHTML +=\"", col);

    if (bold)
    printf("<b>");
    for (i = 0; cptr[i] != '\0'; i++)
    {
        switch (cptr[i])
        {
            case '>':
            printf("&gt");
            break;
            case '<':
            printf("&lt");
            break;
            case '\r':
            break;
            case '\n':
            printf("<br>");
            break;
            case '"':
            printf("%s", "&quot;");
            break;
            default:
            printf("%c", cptr[i]);
            break;
        }
    }
    if(bold)
    printf("</b>");

    printf("\";</script>\n");

    fflush(stdout);
    return i;
}
int readline(int fd,char *ptr,int maxlen, char delim)
{
    int n,rc;
    char c;
    *ptr = 0;
    for(n=1; n<maxlen; n++)
    {
        if((rc=read(fd,&c,1)) == 1)
        {
            *ptr++ = c;
            if(c==' '&& *(ptr-2) == delim) { break; }
            if(c=='\n') break;
        }
        else if(rc==0)
        {
            if(n==1) return(0);  //dont read any char
            else break;

        }
        else
        {
            if (errno == EAGAIN)
            {
                sleep(1);
                n--;
                continue;
            }
            return(-1);
        }

    }
    return(n);
}

void html_header()
{
    printf("%s",
    "<html>\r\n\
    <head>\r\n\
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />\r\n\
    <title>Network Programming Homework 3</title>\r\n\
    </head>\r\n\
    <body bgcolor=#336699>\r\n\
    <font face=\"Courier New\" size=2 color=#FFFF99>\r\n"
);
fflush(stdout);
}
